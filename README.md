Hi, My name is April from a little one studio, your best photographer specializing in Newborn photography. I offer both studio and home-visit session for newborns with plenty of props and outfits and create the best memories for your little precious babies.

Website: https://alittleonetoronto.com/

